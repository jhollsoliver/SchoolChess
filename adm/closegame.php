<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;



require 'vendor/autoload.php';

$client = new Client(new Version1X('http://localhost:3000'));
$client->initialize();
$e = $client->emit('adm_closegame', ['gameId' => $argv[1]]);
$r = $client->read();
var_dump($r);
$client->close();

